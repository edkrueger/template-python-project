# python-template
A template for a simple python with pre-commit dev tools.

## Setup
Run `pipenv install` to install the env.
Run `pipenv run pre-commit install` to initialize the git hooks.
Run `pipenv run pre-commit run --all-files` if there are file that were committed before adding the git hooks.

## Use
Activate the shell with: `pipenv shell`